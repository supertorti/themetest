<?php

namespace ThemeTest\Providers;

use Plenty\Plugin\ServiceProvider;
use Plenty\Plugin\Templates\Twig;
use Plenty\Plugin\Events\Dispatcher;
use IO\Extensions\Functions\Partial;
use IO\Helper\TemplateContainer;

class ThemeTestServiceProvider extends ServiceProvider
{

    const PRIORITY = 0;

    /**
     * Register the service provider.
     */
    public function register()
    {

    }


    /**
     * @param Twig $twig
     * @param Dispatcher $eventDispatcher
     * @return bool
     */
    public function boot(Twig $twig, Dispatcher $eventDispatcher)
    {
        // Override footer
        $eventDispatcher->listen('IO.init.templates', function(Partial $partial)
        {
            $partial->set('footer', 'ThemeTest::partial.myfooter');
        }, self::PRIORITY);


        // Override homepage
/*        $eventDispatcher->listen('IO.tpl.home', function (TemplateContainer $container)
        {
            $container->setTemplate('ThemeTest::Homepage.myHomepage');
            return false;
        }, self::PRIORITY);*/

        return false;
    }
}