<?php

namespace ThemeTest\Containers;

use Plenty\Plugin\Templates\Twig;

class ThemeTestContainer
{
    public function call(Twig $twig):string
    {
        return $twig->render('ThemeTest::content.ThemeTest');
    }
}